import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class AlphabetSoup {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please enter input file name.");
            return;
        }

        String inputFileName = args[0];
        ArrayList<String> wordsToFind = new ArrayList<>();
        char[][] grid = null;
        int rows = 0;
        int cols = 0;

        try (Scanner scanner = new Scanner(new File(inputFileName))) {
            String line = scanner.nextLine();
            String[] dimensions = line.split("x");
            rows = Integer.parseInt(dimensions[0]);
            cols = Integer.parseInt(dimensions[1]);
            grid = new char[rows][cols];

            for (int i = 0; i < rows; i++) {
                line = scanner.nextLine();
                String[] chars = line.split(" ");
                for (int j = 0; j < cols; j++) {
                    grid[i][j] = chars[j].charAt(0);
                }
            }

            while (scanner.hasNextLine()) {
                String word = scanner.nextLine();
                wordsToFind.add(word);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + inputFileName);
            return;
        }

        for (String word : wordsToFind) {
            boolean found = false;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    if (grid[i][j] == word.charAt(0)) {
                        for (int directionX = -1; directionX <= 1; directionX++) {
                            for (int directionY = -1; directionY <= 1; directionY++) {
                                if (directionX == 0 && directionY == 0) {
                                    continue;
                                }

                                int currentX = j;
                                int currentY = i;
                                int k;

                                for (k = 1; k < word.length(); k++) {
                                    currentX += directionX;
                                    currentY += directionY;

                                    if (currentX < 0 || currentY < 0 || currentX >= cols || currentY >= rows) {
                                        break;
                                    }

                                    if (grid[currentY][currentX] != word.charAt(k)) {
                                        break;
                                    }
                                }

                                if (k == word.length()) {
                                    System.out.printf("%s %d:%d %d:%d\n", word, i, j, currentY, currentX);
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                    }
                    if (found) {
                        break;
                    }
                }
            }
        }
    }
}
